---

- name: Fail if OS is not Debian based
  ansible.builtin.fail:
    msg: "Role only support Debian based distributions"
  when: ansible_pkg_mgr != "apt"

- name: Update apt cache
  ansible.builtin.apt:
    update-cache: true

- name: Install Utility software
  ansible.builtin.apt:
    name: "{{ item }}"
    state: latest
  with_items:
    - software-properties-common
    - python-mysqldb

- name: Make sure pymysql is present
  ansible.builtin.pip:
    name: pymysql
    state: present


- name: Install packages
  ansible.builtin.apt:
    pkg: "mariadb-server"
    state: present
  register: result
  notify: restart MySQL

- name: Start MariaDB
  ansible.builtin.command: service mysql start

- name: Is root password set?
  command: mysql -u root --execute "SELECT NOW()"
  register: is_root_password_set
  ignore_errors: true

- name: MySQL secure installation
  block:
  - name: Update MySQL root password
    community.mysql.mysql_user:
      name: root
      host: "{{ item }}"
      password: "{{ mysql_root_pass }}"
      login_unix_socket: /run/mysqld/mysqld.sock
    with_items:
      - 127.0.0.1
      - ::1
      - localhost
    when: is_root_password_set.rc == 0

  - name: Delete anonymous MySQL user
    community.mysql.mysql_user:
      name: ""
      host: localhost
      state: absent
      login_password: "{{ mysql_root_pass }}"
      login_unix_socket: /run/mysqld/mysqld.sock
    with_items:
      - localhost
      - "{{ ansible_nodename }}"

  - name: Delete Hostname based MySQL user
    community.mysql.mysql_user:
      name: root
      host: "{{ansible_nodename}}"
      state: absent
      login_password: "{{ mysql_root_pass }}"
      login_unix_socket: /run/mysqld/mysqld.sock

  - name: Remove MySQL test database
    community.mysql.mysql_db:
      name: test
      state: absent
      login_password: "{{ mysql_root_pass }}"
      login_unix_socket: /run/mysqld/mysqld.sock

- name: Create devops_express database
  community.mysql.mysql_db:
    name: devops_express
    state: present
    login_password: "{{ mysql_root_pass }}"

- name: Create database user with name 'bob' and password '12345' with all devops_express privileges
  community.mysql.mysql_user:
    name: bob
    password: 12345
    priv: 'devops_express.*:ALL'
    state: present
    login_password: "{{ mysql_root_pass }}"

- name: Create table
  community.mysql.mysql_query:
    login_db: devops_express
    login_user: bob
    login_password: 12345
    query: >-
      CREATE TABLE IF NOT EXISTS Persons (
      person_id int,
      person_first_name varchar(255),
      person_city varchar(255)
      )
    # -> CREATE TABLE Persons ( person_id int, person_first_name varchar(255),[...]

- name: Insert values
  community.mysql.mysql_query:
    login_db: devops_express
    login_user: bob
    login_password: 12345
    query:
      - INSERT INTO Persons (person_id, person_first_name, person_city) VALUES (1, 'Jaimie', 'Cugnaux')
      - INSERT INTO Persons (person_id, person_first_name, person_city) VALUES (1, 'Andy', 'Saint-jory')

- name: Select all
  community.mysql.mysql_query:
    login_db: devops_express
    login_user: bob
    login_password: 12345
    query: SELECT * FROM Persons

- name: Select names for Cugnaux inhabitants
  community.mysql.mysql_query:
    login_db: devops_express
    login_user: bob
    login_password: 12345
    query: >-
      SELECT person_first_name
      FROM Persons
      WHERE person_city='Cugnaux'
