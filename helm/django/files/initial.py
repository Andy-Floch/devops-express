import os
import time

from django.db import connection

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "blog.settings")


def check_db():
    """
    检测数据库连
    :return:
    """
    with connection.cursor() as cursor:
        # cursor.execute("show tables;")
        pass


def initial_job():
    """
    执行任务：
      - 数据模型初始化
      - 创建超级管理员
    :return:
    """
    # os.system('python3 -V')
    # 一定要用 export 倒入变量

    # 通过环境变量方式设置超级账号密码
    # os.environ.setdefault("DJANGO_SUPERUSER_PASSWORD", "changeme")
    # export DJANGO_SUPERUSER_PASSWORD = changeme

    os.system('\
        python3 manage.py makemigrations && python3 manage.py migrate && \
        python3 manage.py createsuperuser --username $DJANGO_SUPERUSER_USERNAME --email $DJANGO_SUPERUSER_MAIL --noinput \
    ')


def main():
    try:
        # 测试数据库连接
        check_db()
    except Exception as e:
        now = time.strftime('%Y-%m-%d %H:%M:%S', time.localtime(time.time()))
        print(now, e)
        time.sleep(5)
        main()
    else:
        # 初始化数据
        initial_job()


if __name__ == "__main__":
    main()
