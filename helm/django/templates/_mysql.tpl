{{/* 帮助返回 MySQL 名称 */}}
{{- define "helm-chart.mysql.name" -}}
{{- printf "%s-%s" .Release.Name  .Values.mysql.name | trunc 63 | trimSuffix "-" -}}
{{- end -}}

{{/* 帮助返回 MySQL 地址 */}}
{{- define "helm-chart.mysql.host" -}}
{{- if .Values.mysql.enabled }}
{{- printf "%s-%s" .Release.Name  .Values.mysql.name | trunc 63 | trimSuffix "-" -}}
{{- else }}
{{- required "需要一个有效的 .Values.mysql.externalMySQL.host 条目！" .Values.mysql.externalMySQL.host }}
{{- end }}
{{- end -}}

{{/* 帮助返回 MySQL 端口 */}}
{{- define "helm-chart.mysql.port" -}}
{{- if .Values.mysql.enabled }}
{{- .Values.mysql.service.port -}}
{{- else }}
{{- required "需要一个有效的 .Values.mysql.externalMySQL.port 条目！" .Values.mysql.externalMySQL.port }}
{{- end }}
{{- end -}}

{{/* 帮助返回 MySQL 用户名 */}}
{{- define "helm-chart.mysql.user" -}}
{{- if .Values.mysql.enabled }}
{{- default "blog" .Values.mysql.auth.user }}
{{- else }}
{{- required "需要一个有效的 .Values.mysql.externalMySQL.user 条目！" .Values.mysql.externalMySQL.user }}
{{- end }}
{{- end -}}

{{/* 帮助返回 MySQL 密码 */}}
{{- define "helm-chart.mysql.password" -}}
{{- if .Values.mysql.enabled }}
{{- default "password" .Values.mysql.auth.password }}
{{- else }}
{{- required "需要一个有效的 .Values.mysql.externalMySQL.password 条目！" .Values.mysql.externalMySQL.password }}
{{- end }}
{{- end -}}

{{/* 帮助返回 MySQL root 密码 */}}
{{- define "helm-chart.mysql.root_password" -}}
{{- if .Values.mysql.enabled }}
{{- default "password" .Values.mysql.auth.root_password | quote}}
{{- end }}
{{- end -}}

{{/* 帮助返回 MySQL 数据库 */}}
{{- define "helm-chart.mysql.database" -}}
{{- if .Values.mysql.enabled }}
{{- default "blog" .Values.mysql.auth.database }}
{{- else }}
{{- required "需要一个有效的 .Values.mysql.externalMySQL.database 条目！" .Values.mysql.externalMySQL.database }}
{{- end }}
{{- end -}}

{{/* 帮助返回 MySQL fullname */}}
{{- define "helm-chart.mysql.fullname" -}}
{{- printf "%s-%s" "mysql" .Release.Name | trunc 63 | trimSuffix "-" -}}
{{- end -}}

{{/* 帮助返回 Selector Labels */}}
{{- define "helm-chart.mysql.selectorLabels" -}}
app.kubernetes.io/name: {{ template "helm-chart.mysql.fullname" . }}
app.kubernetes.io/instance: {{ .Release.Name }}
{{- end }}

{{/* 帮助返回 通用 Labels */}}
{{- define "helm-chart.mysql.labels" -}}
helm.sh/chart: {{ include "helm-chart.chart" . }}
{{ include "helm-chart.mysql.selectorLabels" . }}
{{- if .Chart.AppVersion }}
app.kubernetes.io/version: {{ .Chart.AppVersion | quote }}
{{- end }}
app.kubernetes.io/managed-by: {{ .Release.Service }}
{{- end }}