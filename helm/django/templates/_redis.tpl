{{/* 帮助返回 Redis 名称 */}}
{{- define "helm-chart.redis.name" -}}
{{- printf "%s-%s" .Release.Name  .Values.redis.name | trunc 63 | trimSuffix "-" -}}
{{- end -}}

{{/* 帮助返回 Redis 地址 */}}
{{- define "helm-chart.redis.host" -}}
{{- if .Values.redis.enabled }}
{{- printf "%s-%s" .Release.Name  .Values.redis.name | trunc 63 | trimSuffix "-" -}}
{{- else }}
{{- required "需要一个有效的 .Values.redis.externalRedis.host 条目！" .Values.redis.externalRedis.host }}
{{- end }}
{{- end -}}

{{/* 帮助返回 Redis 端口 */}}
{{- define "helm-chart.redis.port" -}}
{{- if .Values.redis.enabled }}
{{- .Values.redis.service.port -}}
{{- else }}
{{- required "需要一个有效的 .Values.redis.externalRedis.port 条目！" .Values.redis.externalRedis.port }}
{{- end }}
{{- end -}}

{{/* 帮助返回 Redis 密码 */}}
{{- define "helm-chart.redis.password" -}}
{{- if .Values.redis.enabled }}
{{- default "foobared" .Values.redis.auth.password }}
{{- else }}
{{- required "A valid .Values.redis.externalRedis.password entry required!" .Values.redis.externalRedis.password }}
{{- end }}
{{- end -}}

{{/* 帮助返回 Redis fullname */}}
{{- define "helm-chart.redis.fullname" -}}
{{- printf "%s-%s" "redis" .Release.Name | trunc 63 | trimSuffix "-" -}}
{{- end -}}

{{/* 帮助返回 Selector Labels */}}
{{- define "helm-chart.redis.selectorLabels" -}}
app.kubernetes.io/name: {{ template "helm-chart.redis.fullname" . }}
app.kubernetes.io/instance: {{ .Release.Name }}
{{- end }}

{{/* 帮助返回 通用 Labels */}}
{{- define "helm-chart.redis.labels" -}}
helm.sh/chart: {{ include "helm-chart.chart" . }}
{{ include "helm-chart.redis.selectorLabels" . }}
{{- if .Chart.AppVersion }}
app.kubernetes.io/version: {{ .Chart.AppVersion | quote }}
{{- end }}
app.kubernetes.io/managed-by: {{ .Release.Service }}
{{- end }}