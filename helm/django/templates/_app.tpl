{{/* 帮助返回 Redis 名称 */}}
{{- define "helm-chart.mysql.name" -}}
{{- printf "%s-%s" .Release.Name  .Values.mysql.name | trunc 63 | trimSuffix "-" -}}
{{- end -}}

{{/* 帮助返回 MySQL 地址 */}}
{{- define "helm-chart.mysql.host" -}}
{{- if .Values.mysql.enabled }}
{{- printf "%s-%s" .Release.Name  .Values.mysql.name | trunc 63 | trimSuffix "-" -}}
{{- else }}
{{- required "需要一个有效的 .Values.mysql.externalMySQL.host 条目！" .Values.mysql.externalMySQL.host }}
{{- end }}
{{- end -}}

{{/* 帮助返回 MySQL 端口 */}}
{{- define "helm-chart.mysql.port" -}}
{{- if .Values.mysql.enabled }}
{{- .Values.mysql.service.port -}}
{{- else }}
{{- required "需要一个有效的 .Values.mysql.externalMySQL.port 条目！" .Values.mysql.externalMySQL.port }}
{{- end }}
{{- end -}}

{{/* 帮助返回 MySQL 用户名 */}}
{{- define "helm-chart.mysql.user" -}}
{{- if .Values.mysql.enabled }}
{{- default "blog" .Values.mysql.auth.user }}
{{- else }}
{{- required "需要一个有效的 .Values.mysql.externalMySQL.user 条目！" .Values.mysql.externalMySQL.user }}
{{- end }}
{{- end -}}

{{/* 帮助返回 MySQL 密码 */}}
{{- define "helm-chart.mysql.password" -}}
{{- if .Values.mysql.enabled }}
{{- default "password" .Values.mysql.auth.password }}
{{- else }}
{{- required "需要一个有效的 .Values.mysql.externalMySQL.password 条目！" .Values.mysql.externalMySQL.password }}
{{- end }}
{{- end -}}

{{/* 帮助返回 MySQL root 密码 */}}
{{- define "helm-chart.mysql.root_password" -}}
{{- if .Values.mysql.enabled }}
{{- default "password" .Values.mysql.auth.root_password | quote}}
{{- end }}
{{- end -}}

{{/* 帮助返回 MySQL 数据库 */}}
{{- define "helm-chart.mysql.database" -}}
{{- if .Values.mysql.enabled }}
{{- default "blog" .Values.mysql.auth.database }}
{{- else }}
{{- required "需要一个有效的 .Values.mysql.externalMySQL.database 条目！" .Values.mysql.externalMySQL.database }}
{{- end }}
{{- end -}}


{{/* 帮助返回 Redis 名称 */}}
{{- define "helm-chart.redis.name" -}}
{{- printf "%s-%s" .Release.Name  .Values.redis.name | trunc 63 | trimSuffix "-" -}}
{{- end -}}

{{/* 帮助返回 Redis 地址 */}}
{{- define "helm-chart.redis.host" -}}
{{- if .Values.redis.enabled }}
{{- printf "%s-%s" .Release.Name  .Values.redis.name | trunc 63 | trimSuffix "-" -}}
{{- else }}
{{- required "需要一个有效的 .Values.redis.externalRedis.host 条目！" .Values.redis.externalRedis.host }}
{{- end }}
{{- end -}}

{{/* 帮助返回 Redis 端口 */}}
{{- define "helm-chart.redis.port" -}}
{{- if .Values.redis.enabled }}
{{- .Values.redis.service.port -}}
{{- else }}
{{- required "需要一个有效的 .Values.redis.externalRedis.port 条目！" .Values.redis.externalRedis.port }}
{{- end }}
{{- end -}}

{{/* 帮助返回 Redis 密码 */}}
{{- define "helm-chart.redis.password" -}}
{{- if .Values.redis.enabled }}
{{- default "foobared" .Values.redis.auth.password }}
{{- else }}
{{- required "A valid .Values.redis.externalRedis.password entry required!" .Values.redis.externalRedis.password }}
{{- end }}
{{- end -}}