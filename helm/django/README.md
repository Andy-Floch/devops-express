## 小兵的 Django 包
Django是一个有期限的、完美主义者的网络框架！

整合了 Django + MySQL + Redis + Gunicorn + 阿里云OSS + Django-rest-framework

DEMO: https://labdoc.cc/

特别说明：MySQL和Redis未做持久化操作，建议使用外部服务提供。

免责声明：Django是Django有限公司的注册商标。其中的任何权利均归Django有限公司所有。我们的任何使用仅供参考，并不表示Django有限公司之间有任何赞助、背书或从属关系。

## 安装、部署
```bash
$ helm repo add ju4t https://charts.labdoc.cc
$ helm install demo ju4t/django
```
通过 *--set key=value[,key=value]* 来指定可选参数，可选参数见 【Default values】

例如：
```bash
$ helm repo add ju4t https://charts.labdoc.cc
$ helm install demo ju4t/django --set app.auth.username=admin,app.auth.password=changeme
```